import { checkAuth } from './utils/auth'

export async function handler(event, context) {
    try {
        const user = await checkAuth(context);
        console.log(user);
        return {
            statusCode: 200,
            body: 'Hello, world auth!!',
        };
    } catch (e) {
        return {
            statusCode: 403,
            body: e.message,
        };
    }
};
