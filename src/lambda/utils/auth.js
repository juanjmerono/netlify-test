/* Check context for user */
export async function checkAuth(context) {
    return new Promise((resolve, reject) => {
      // Reading the context.clientContext will give us the current user
      const user = context.clientContext && context.clientContext.user
      if (!user) {
        return reject(new Error('Unauthorized access'))
      }
      return resolve(user)
    })
}