import faunadb from 'faunadb'
import { checkAuth } from './utils/auth'

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

export async function handler(event, context) {
    try {
        // Only auth users can do this
        const user = await checkAuth(context);
        try {
            const response = await client.query(q.Paginate(q.Match(q.Ref('indexes/all_rubricas'))));
            const rubricsRefs = response.data;
            console.log('Rubrics refs', rubricsRefs);
            console.log(`${rubricsRefs.length} teams found`);
            const getAllRubricsDataQuery = rubricsRefs.map((ref) => {
                return q.Get(ref)
            });
            const ret = await client.query(getAllRubricsDataQuery);
            return {
                statusCode: 200,
                body: JSON.stringify(ret),
            };
        } catch (error) {
            return {
                statusCode: 404,
                body: JSON.stringify(e),
            };
        }
    } catch (e) {
        return {
            statusCode: 403,
            body: e.message,
        };
    }
};
