function render(template,node, override) {
    if (override) {
        node.innerHTML = template;
    } else {
        node.innerHTML += template;
    }
}

function loadTeams() {
    fetch("/.netlify/functions/teams", {
        headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + netlifyIdentity.currentUser().token.access_token,
        },
    }).then(function(response){
        if (response.status !== 200) {
            var template = '<h3> Error cargando datos: ' + response.status + ' </h3>';
            render(template, document.querySelector('#error'));
            return;
        }
        fetch("/.netlify/functions/rubrics", {
            headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + netlifyIdentity.currentUser().token.access_token,
            },
        }).then(function(response2){
            if (response2.status !== 200) {
                var template = '<h3> Error cargando datos: ' + response2.status + ' </h3>';
                render(template, document.querySelector('#error'));
                return;
            }
            response2.json().then(function(data1) {
                for (var idx in data1) {
                    var template1 = '<th>'+data1[idx].data.name+'</th>';
                    render(template1, document.querySelector('#rubrics'));
                }
                response.json().then(function(data) {
                    for (var id in data) {
                        var template = '<tr>'+
                            '<td>'+data[id].data.id+'</td>'+
                            '<td>'+data[id].data.name+'</td>';
                        for (var idx in data1) {
                            template += '<td><input type="number" size="3" name="r'+data1[idx].data.id+'_'+data[id].data.id+'"/></td>';
                        }
                        template += '</tr>';
                        render(template, document.querySelector('#teams'));
                    }
                });
            });
        }).catch(function(err2){
            console.log('Fetch Error2 :-S', err2);
        });
    }).catch(function(err){
        console.log('Fetch Error :-S', err);
    });
}

netlifyIdentity.on('login', user => {
    console.log('login',user)
    loadTeams();
});
netlifyIdentity.on('logout', () => {
    console.log('logout',user)
});
//netlifyIdentity.on('error', err => console.error('Error', err));
//netlifyIdentity.on('open', () => console.log('Widget opened'));
//netlifyIdentity.on('close', () => console.log('Widget closed'));
